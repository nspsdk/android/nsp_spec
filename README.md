# NSP_Spec

## Requirements
---

This SDK works with an **APP ID** provided by [SmartProfile](https://www.smartp.com/c.dspHome/smartprofile/)



## Integrating the SDK
---

- Copy the library in the **libs** folder
- File > Propect Structure
- Click on « Dependencies »
- Click on « app » then on the « + » button on the right
- Click on « JAR/AAR Package »
- Enter the path of the package *(libs/nsp_sdk.aar)*


## Initialize the SDK
---

**Java**
```java
NspCore core = new NspCore("APP_ID", /* NspCore.GDPROptions */, this);
core.setLogin("LOGIN");
```


**Kotlin**
```kotlin
val core: NspCore = NspCore("APP_ID", /* NspCore.GDPROptions */, this)
core.setLogin("LOGIN")
```



# GDPROptions values
---

- Full : All informations will be recorded.

- Anon : All informations will be anonymized before being recorded.

- None : No informations will be recorded.


# Push Notifications initial configuration
---

Send us the **Android Package Name** for configuration.

Next add those Firebase SDKs :

**Module (app-level) Gradle file (\<project>/\<app-module>/build.gradle)**

**Java**
```java
plugins {
  id 'com.android.application'

  ...
}

dependencies {
  // Import the Firebase BoM
  implementation platform('com.google.firebase:firebase-bom:31.2.3')

  // When using the BoM, don't specify versions in Firebase dependencies
  implementation 'com.google.firebase:firebase-messaging'
}
```

**Kotlin**
```kotlin
plugins {
  id 'com.android.application'

  ...
}

dependencies {
  // Import the Firebase BoM
  implementation platform('com.google.firebase:firebase-bom:31.2.3')

  // When using the BoM, don't specify versions in Firebase dependencies
  implementation 'com.google.firebase:firebase-messaging-ktx'
}
```

Next, you need to create a **Firebase Messaging Service** in order to implement those 2 methods :
- **onMessageReceived**
- **onNewToken**

**Java**
```Java
public class CustomFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
		super.onMessageReceived(remoteMessage);

        // Present notification and / or send tracking normal tracking event with NspSdk
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);

		// Call setNotificationToken on NspSdk instance in order to update the user device token in the database
        core.setNotificationToken(token);
    }
}
```

**Kotlin**
```kotlin
class CustomFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        // Present notification and / or send tracking normal tracking event with NspSdk
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        // Call setNotificationToken on NspSdk instance in order to update the user device token in the database
        core.setNotificationToken(token)
    }
}
```

Finally, you can initiate the Firebase SDK and update the token with this code, in the desired Activity / Fragment.

**Java**
```java
NspCore core = new NspCore("APP_ID", /* NspCore.GDPROptions */, this);
core.setLogin("LOGIN");

// Prepare options using NspSdk configuration
FirebaseOptions options = new FirebaseOptions.Builder()
                .setApiKey("API_KEY")
                .setGcmSenderId("GCM_SENDER_ID")
                .setApplicationId("APP_ID")
                .setProjectId("PROJECT_ID")
                .build();

// Initialize App
FirebaseApp.initializeApp(getApplicationContext(), options);

// Requestion notification authorization and update the user device token in the database
FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
	if (!task.isSuccessful()) {
		return;
	}

	final String token = task.getResult();
	core.setNotificationToken(token);
});
```

**Kotlin**
```kotlin
val core: NspCore = NspCore("APP_ID", /* NspCore.GDPROptions */, this)
core.setLogin("LOGIN")

// Prepare options using NspSdk configuration
val options = FirebaseOptions.Builder()
                .setApiKey("API_KEY")
                .setGcmSenderId("GCM_SENDER_ID")
                .setApplicationId("APP_ID")
                .setProjectId("PROJECT_ID")
                .build()

// Initialize App
FirebaseApp.initializeApp(applicationContext, options)

// Requestion notification authorization and update the user device token in the database
FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
    if (!task.isSuccessful) {
        return@addOnCompleteListener
    }

    val token = task.result
    core.setNotificationToken(token)
}
```

## Author

devmobile@nsp-fr.com



## License

NSP_SDK is available under the MIT license. See the LICENSE file for more info.
